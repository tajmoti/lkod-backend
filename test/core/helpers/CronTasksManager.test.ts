import { expect } from "chai";
import "mocha";
import * as sinon from "sinon";
import { cronTasksManger, log } from "../../../src/core/helpers";

describe("CronTasksManger", () => {
    let sandbox: sinon.SinonSandbox;
    let testFunc: sinon.SinonStub;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        testFunc = sandbox.stub();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should has registerTask method", async () => {
        expect(cronTasksManger.registerTask).not.to.be.undefined;
    });

    it("should has checkStatus method", async () => {
        expect(cronTasksManger.checkStatus).not.to.be.undefined;
    });

    it("should has startAll method", async () => {
        expect(cronTasksManger.startAll).not.to.be.undefined;
    });

    it("should has stopAll method", async () => {
        expect(cronTasksManger.stopAll).not.to.be.undefined;
    });

    it("should register new task", () => {
        cronTasksManger.registerTask({
            cronTime: "*/2 * * * * *",
            name: "test",
            process: () => {
                log.debug("I'm running.");
                testFunc();
                return Promise.resolve();
            },
        });
        expect(Object.keys(cronTasksManger.getRunningTasks())).to.be.an("array").that.is.empty;
        expect(Object.keys(cronTasksManger.getStoppedTasks())).to.be.an("array").that.is.not.empty;
        cronTasksManger.checkStatus();
    });

    it("should start all", async () => {
        cronTasksManger.startAll();
        expect(Object.keys(cronTasksManger.getRunningTasks())).to.be.an("array").that.is.not.empty;
        expect(Object.keys(cronTasksManger.getStoppedTasks())).to.be.an("array").that.is.empty;
        cronTasksManger.checkStatus();
        await new Promise((resolve) => setTimeout(resolve, 2000));
        sandbox.assert.calledOnce(testFunc);
    });

    it("should stop all", () => {
        cronTasksManger.stopAll();
        expect(Object.keys(cronTasksManger.getRunningTasks())).to.be.an("array").that.is.empty;
        expect(Object.keys(cronTasksManger.getStoppedTasks())).to.be.an("array").that.is.not.empty;
        cronTasksManger.checkStatus();
    });

    it("should handle throwable", async () => {
        cronTasksManger.registerTask({
            cronTime: "*/2 * * * * *",
            name: "test",
            process: () => {
                log.debug("I'm running.");
                throw new Error("I'm failed.");
            },
        });
        cronTasksManger.startAll();
        cronTasksManger.checkStatus();
        await new Promise((resolve) => setTimeout(resolve, 2000));
        cronTasksManger.stopAll();
    });
});
