import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import axios from "axios";
import MockAdapter from "axios-mock-adapter";
import { expect } from "chai";
import "mocha";
import { config } from "../../../src/core/config";
import { SparqlConnectionStrategy } from "../../../src/core/strategies";

describe("SparqlConnectionStrategy", () => {
    let mock: MockAdapter;

    before(() => {
        mock = new MockAdapter(axios);
    });

    afterEach(() => {
        mock.reset();
    });

    after(() => {
        mock.restore();
    });

    it("should upload dataset to sparql", async () => {
        mock.onPost(config.sparql.data_url).reply(200);
        try {
            const res = await SparqlConnectionStrategy.uploadDataset({ data: "data" });
            expect(res).to.be.true;
        } catch (err) {
            expect(err).to.be.undefined;
        }
    });

    it("should throw error if dataset uploading failed", async () => {
        mock.onPost(config.sparql.data_url).reply(400);
        try {
            const res = await SparqlConnectionStrategy.uploadDataset({ data: "bad data" });
        } catch (err) {
            expect(err).to.be.instanceOf(CustomError);
        }
    });

    it("should remove dataset cascade from sparql", async () => {
        mock.onPost(config.sparql.update_url).reply(204);
        try {
            const res = await SparqlConnectionStrategy.removeDatasetCascade("data");
            expect(res).to.be.true;
        } catch (err) {
            expect(err).to.be.undefined;
        }
    });

    it("should throw error if dataset removing failed", async () => {
        mock.onPost(config.sparql.update_url).reply(400);
        try {
            const res = await SparqlConnectionStrategy.removeDatasetCascade("bad data");
        } catch (err) {
            expect(err).to.be.instanceOf(CustomError);
        }
    });

    it("should upload vocabulary to sparql", async () => {
        const sourceUrl = "http://publications.europa.eu/resource/distribution/data-theme/rdf/skos_core/data-theme-skos.rdf";
        const contentType = "application/rdf+xml";
        const iriPrefixes = ["http://publications.europa.eu/resource/distribution/data-theme"];
        mock.onGet(sourceUrl).reply(200, "test");
        mock.onPost(config.sparql.update_url).reply(204);
        mock.onPost(config.sparql.data_url).reply(200);
        try {
            await SparqlConnectionStrategy.uploadVocabularies([{ id: "test", sourceUrl, contentType, iriPrefixes }]);
        } catch (err) {
            expect(err).to.be.undefined;
        }
    });

    it("should throw error if upload vocabulary to sparql failed", async () => {
        const sourceUrl = "http://test";
        const contentType = "application/rdf+xml";
        mock.onGet(sourceUrl).reply(404);
        // mock.onPost(config.sparql.data_url).reply(200);
        try {
            await SparqlConnectionStrategy.uploadVocabularies([{ id: "test", sourceUrl, contentType, iriPrefixes: [] }]);
        } catch (err) {
            expect(err).to.be.instanceOf(CustomError);
        }
    });

    it("should upload vocabulary to sparql", async () => {
        const sourceUrl = "http://publications.europa.eu/resource/distribution/data-theme/rdf/skos_core/data-theme-skos.rdf";
        const contentType = "application/rdf+xml";
        mock.onGet(sourceUrl).reply(200, "test", { "content-type": "application/rdf+xml" });
        mock.onPost(config.sparql.data_url).reply(413);
        try {
            await SparqlConnectionStrategy.uploadVocabularies([{ id: "test", sourceUrl, contentType, iriPrefixes: [] }]);
        } catch (err) {
            expect(err).to.be.instanceOf(CustomError);
        }
    });
});
