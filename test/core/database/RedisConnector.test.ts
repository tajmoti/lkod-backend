import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { expect } from "chai";
import "mocha";
import { redisConnector } from "../../../src/core/database";

describe("RedisConnector", () => {
    it("should has connect method", async () => {
        expect(redisConnector.connect).not.to.be.undefined;
    });

    it("should has getConnection method", async () => {
        expect(redisConnector.getConnection).not.to.be.undefined;
    });

    it("should throws Error if not connect method was not called", () => {
        expect(redisConnector.getConnection).to.throw(CustomError);
    });

    it("should connects to Redis and returns connection", async () => {
        const ch = await redisConnector.connect();
        expect(ch).to.be.an.instanceof(Object);
    });

    it("should returns connection", async () => {
        await redisConnector.connect();
        expect(redisConnector.getConnection()).to.be.an.instanceof(Object);
    });
});
