import { config } from "../../src/core/config";
import { postgresConnector, redisConnector } from "../../src/core/database";
import { initModels } from "../../src/models";

const DBMigrate = require("db-migrate");
const dbmigrate = DBMigrate.getInstance(true, {
    cmdOptions: {
        env: "test",
        "migrations-dir": "db/migrations",
    },
});

export const init = async () => {
    // set postgres db schema to `test`
    config.database.schema = "test";

    // connect to postgres db
    const connection = await postgresConnector.connect();

    // create `test` schema in postgres db
    await connection.query(`DROP SCHEMA IF EXISTS ${config.database.schema} CASCADE;`);
    await connection.query(`CREATE SCHEMA ${config.database.schema};`);
    await connection.query(`SET search_path TO ${config.database.schema};`);

    // run migrations
    await dbmigrate.up();

    // init models
    initModels();

    // add test data
    await connection.query(
        `` +
            `INSERT INTO "user" ("email", "password", "name", "role", "hasDefaultPassword") VALUES` +
            `    ('test@golemio.cz','$2b$10$PCAwdjhktHpwOBJBW7RIFeGD8IYXiSMIohRdG83hl8j.lerlWOwP.','test user','user','false');` +
            `INSERT INTO "organization" ("name", "identificationNumber", "slug", "logo", "description") VALUES` +
            `    ('test org', '02795281', 'test-org', 'https://placehold.co/100x100', ` +
            `       'Lorem ipsum dolor sit amet.');` +
            `INSERT INTO "user_organization" ("userId", "organizationId") VALUES` +
            `    ('2','1');` +
            `INSERT INTO "user" ("email", "password", "name", "role", "hasDefaultPassword") VALUES` +
            `    ('admin@golemio.cz','$2b$10$Wsvnq8mxPWuV4XpWlMijWOX7m6Dy3kPRwW.qgL9x95NWvHdL0JEoC','test admin'` +
            `    ,'admin','false');` +
            `INSERT INTO "organization" ("name", "identificationNumber", "slug") VALUES` +
            `    ('second test org', '02795281', 'second-test-org');` +
            `INSERT INTO "organization" ("name", "identificationNumber", "slug", "arcGisFeed") VALUES` +
            `    ('arcgis test org', '02795282', 'arcgis-test-org', 'https://test-data-brno-cz-mestobrno.hub.arcgis.com/api/feed/dcat-us/1.1.json');` +
            `INSERT INTO "user_organization" ("userId", "organizationId") VALUES` +
            `    ('2','3');` +
            `INSERT INTO "user" ("email", "password", "name", "role", "hasDefaultPassword") VALUES` +
            `    ('new@golemio.cz','$2b$10$PCAwdjhktHpwOBJBW7RIFeGD8IYXiSMIohRdG83hl8j.lerlWOwP.','new user','user','true');`
    );

    // connect to redis
    await redisConnector.connect();
};
