import { CustomError, HTTPErrorHandler, ICustomErrorObject } from "@golemio/core/dist/shared/golemio-errors";
import * as chai from "chai";
import * as express from "express";
import { NextFunction, Request, Response } from "express";
import "mocha";
import * as sinon from "sinon";
import * as supertest from "supertest";
import { config } from "../../src/core/config";
import { log } from "../../src/core/helpers";
import { AuthRouter } from "../../src/resources/auth";
import { OrganizationsRouter } from "../../src/resources/organizations";
import { init } from "./00_init.test";
import { OrganizationRepository } from "../../src/models/OrganizationRepository";

describe("Users and Organizations", () => {
    let sandbox: sinon.SinonSandbox;
    // Create clean express instance
    const app = express();
    let authRouter: AuthRouter;
    let organizationsRouter: any; // :any for access to private properties
    let accessToken: string;
    let orgRepo: OrganizationRepository | null;

    before(async () => {
        // mainly database initialization
        await init();

        authRouter = new AuthRouter();
        organizationsRouter = new OrganizationsRouter();

        app.use(express.json());
        app.use(express.urlencoded({ extended: true }));
        // Mount the tested router to the express instance
        app.use("/auth", authRouter.router);
        app.use("/organizations", organizationsRouter.router);
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            // hadle body parser errors
            if (err instanceof SyntaxError) {
                err = new CustomError(err.message, true, "App", (err as any)?.status || 500, err.message);
            }

            const warnCodes = [400, 401, 403, 404, 422];
            const errObject: ICustomErrorObject = HTTPErrorHandler.handle(
                err,
                log,
                warnCodes.includes(err.code) ? "warn" : "error"
            );
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    before((done) => {
        // get access token
        supertest(app)
            .post("/auth/login")
            .send({
                email: "test@golemio.cz",
                password: "pass",
            })
            .end((err: Error | CustomError, res: supertest.Response) => {
                if (err || res.statusCode !== 200) {
                    done(err);
                }
                accessToken = res.body.accessToken;
                done();
            });
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        orgRepo = new OrganizationRepository();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("organizations", () => {
        it("should returns all organizations at GET /organizations", (done) => {
            const url = "/organizations";
            supertest(app)
                .get(url)
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    chai.expect(res.body).to.be.deep.equal([
                        {
                            id: 1,
                            identificationNumber: "02795281",
                            name: "test org",
                            slug: "test-org",
                            logo: "https://placehold.co/100x100",
                            hasArcGisFeed: false,
                            description: "Lorem ipsum dolor sit amet.",
                        },
                        {
                            hasArcGisFeed: true,
                            id: 3,
                            name: "arcgis test org",
                            identificationNumber: "02795282",
                            slug: "arcgis-test-org",
                            logo: null,
                            description: null,
                        },
                    ]);
                    chai.expect(err).to.be.null;
                    done();
                });
        });

        it("should return Organizations as RDF", async () => {
            const res = await orgRepo?.getOrganizationsAsJsonLD();
            chai.expect(res).to.be.deep.equal({
                "@graph": [
                    {
                        "@id": `${config.linked_data.poskytovatel_url_prefix}test-org`,
                        "@type": "schema:Organization",
                        alternateName: "test-org",
                        sameAs: "https://linked.opendata.cz/zdroj/ekonomický-subjekt/02795281",
                        legalName: "test org",
                        name: { "@language": "cs", "@value": "test org" },
                        description: {
                            "@language": "cs",
                            "@value": "Lorem ipsum dolor sit amet.",
                        },
                        logo: "https://placehold.co/100x100",
                    },
                    {
                        "@id": `${config.linked_data.poskytovatel_url_prefix}second-test-org`,
                        "@type": "schema:Organization",
                        alternateName: "second-test-org",
                        sameAs: "https://linked.opendata.cz/zdroj/ekonomický-subjekt/02795281",
                        legalName: "second test org",
                        name: { "@language": "cs", "@value": "second test org" },
                    },
                    {
                        "@id": `${config.linked_data.poskytovatel_url_prefix}arcgis-test-org`,
                        "@type": "schema:Organization",
                        alternateName: "arcgis-test-org",
                        legalName: "arcgis test org",
                        name: {
                            "@language": "cs",
                            "@value": "arcgis test org",
                        },
                        sameAs: "https://linked.opendata.cz/zdroj/ekonomický-subjekt/02795282",
                    },
                ],
                "@context": {
                    alternateName: "http://schema.org/alternateName",
                    description: "http://schema.org/description",
                    logo: { "@id": "http://schema.org/logo", "@type": "@id" },
                    name: "http://xmlns.com/foaf/0.1/name",
                    legalName: "http://schema.org/legalName",
                    sameAs: { "@id": "http://www.w3.org/2002/07/owl#sameAs", "@type": "@id" },
                    schema: "http://schema.org/",
                    rdf: "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
                    owl: "http://www.w3.org/2002/07/owl#",
                    rdfs: "http://www.w3.org/2000/01/rdf-schema#",
                    foaf: "http://xmlns.com/foaf/0.1/",
                },
            });
        });
    });

    describe.skip("users", () => {
        //
    });
});
