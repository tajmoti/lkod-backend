import { CustomError, HTTPErrorHandler, ICustomErrorObject } from "@golemio/core/dist/shared/golemio-errors";
import { Client, FileInfo, FTPError } from "basic-ftp";
import { expect } from "chai";
import * as express from "express";
import { NextFunction, Request, Response } from "express";
import * as path from "path";
import { createSandbox, SinonSandbox, SinonStub } from "sinon";
import * as supertest from "supertest";
import { config } from "../../src/core/config";
import { log } from "../../src/core/helpers";
import { S3Service } from "../../src/core/storage/S3.service";
import { StorageType } from "../../src/core/storage/Storage";
import { SparqlConnectionStrategy } from "../../src/core/strategies";
import { DatasetsRouter } from "../../src/resources/datasets";
import { AuthRouter } from "../../src/resources/auth";
import { init } from "./00_init.test";

describe("Datasets Files", () => {
    let sandbox: SinonSandbox;

    let authRouter: AuthRouter;
    let datasetsRouter: any;

    let accessToken: string;
    let datasetId: string;

    before(async () => {
        // mainly database initialization
        await init();
        sandbox = createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("FTP storage", () => {
        const app = express();
        let removeStub: SinonStub;

        before(async () => {
            sandbox.stub(config, "storageType").value(StorageType.FTP);
            sandbox.stub(config, "ftp").value({
                host: "ftp.test.cz",
                downloadPath: "http://test.cz/public/lkod",
            });

            app.use(express.json());
            app.use(express.urlencoded({ extended: true }));

            authRouter = new AuthRouter();
            datasetsRouter = new DatasetsRouter();

            sandbox.stub(SparqlConnectionStrategy, "uploadDataset").callsFake(() => Promise.resolve(true));

            app.use("/auth", authRouter.router);
            app.use("/datasets", datasetsRouter.router);

            app.use((err: any, req: Request, res: Response, next: NextFunction) => {
                // hadle body parser errors
                if (err instanceof SyntaxError) {
                    err = new CustomError(err.message, true, "App", (err as any)?.status || 500, err.message);
                }

                const warnCodes = [400, 401, 403, 404, 422];
                const errObject: ICustomErrorObject = HTTPErrorHandler.handle(
                    err,
                    log,
                    warnCodes.includes(err.code) ? "warn" : "error"
                );
                log.silly("Error caught by the router error handler.");
                res.setHeader("Content-Type", "application/json; charset=utf-8");
                res.status(errObject.error_status || 500).send(errObject);
            });
        });

        beforeEach(() => {
            sandbox.stub(Client.prototype, "access").resolves();
            sandbox.stub(Client.prototype, "cd").resolves();
            sandbox.stub(Client.prototype, "ensureDir").resolves();
            sandbox.stub(Client.prototype, "close").returns();
            sandbox.stub(Client.prototype, "downloadTo").resolves();
            sandbox.stub(Client.prototype, "uploadFrom").resolves();
            sandbox.stub(Client.prototype, "list").resolves([{ name: "file1.csv" }, { name: "file2.doc" }] as FileInfo[]);
            removeStub = sandbox.stub(Client.prototype, "remove");
        });

        it("should login and create dataset", async () => {
            const loginRes = await supertest(app).post("/auth/login").send({
                email: "test@golemio.cz",
                password: "pass",
            });

            expect(loginRes.status).to.eql(200);
            expect(loginRes.body.accessToken).to.be.a("string");
            accessToken = loginRes.body.accessToken;

            const datasetRes = await supertest(app)
                .post("/datasets")
                .send({
                    organizationId: 1,
                })
                .set("Authorization", `Bearer ${accessToken}`);

            expect(datasetRes.status).to.eql(201);
            expect(datasetRes.body.id).to.be.a("string");
            datasetId = datasetRes.body.id;
        });

        it("should list all dataset files at GET /datasets/:datasetId/files", (done) => {
            supertest(app)
                .get(`/datasets/${datasetId}/files`)
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    expect(res.body).to.eql({
                        files: [
                            `http://test.cz/public/lkod/test-org/${datasetId}/file1.csv`,
                            `http://test.cz/public/lkod/test-org/${datasetId}/file2.doc`,
                        ],
                    });
                    done();
                });
        });

        it("should upload file at POST /datasets/:datasetId/files", (done) => {
            supertest(app)
                .post(`/datasets/${datasetId}/files`)
                .set("Authorization", `Bearer ${accessToken}`)
                .attach("datasetFile", path.join(__dirname, "data/file01.txt"))
                .expect(201)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    expect(res.body).to.eql({ file: `http://test.cz/public/lkod/test-org/${datasetId}/file01.txt` });
                    expect(res.headers.location).to.eql(`http://test.cz/public/lkod/test-org/${datasetId}/file01.txt`);
                    done();
                });
        });

        it("should not allow uploading file at POST /datasets/:datasetId/files", (done) => {
            supertest(app)
                .post(`/datasets/wrongId/files`)
                .set("Authorization", `Bearer ${accessToken}`)
                .attach("datasetFile", path.join(__dirname, "data/file01.txt"))
                .expect(401, done);
        });

        it("should delete file at DELETE /datasets/:datasetId/files/:filename", (done) => {
            removeStub.callsFake(() => null);

            supertest(app)
                .delete(`/datasets/${datasetId}/files/file1.csv`)
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(204, done);
        });

        it("should not allow deleting file at DELETE /datasets/:datasetId/files", (done) => {
            supertest(app)
                .post(`/datasets/wrongId/files`)
                .set("Authorization", `Bearer ${accessToken}`)
                .attach("datasetFile", path.join(__dirname, "data/file01.txt"))
                .expect(401, done);
        });

        it("should return 404 if file does not exist at delete /datasets/:datasetId/files/:filename", (done) => {
            removeStub.rejects(new FTPError({ code: 550, message: "Not found" }));

            supertest(app)
                .delete(`/datasets/${datasetId}/files/file1.csv`)
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(404)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    expect(res.body).to.eql({
                        error_message: "Not Found",
                        error_status: 404,
                    });
                    done();
                });
        });
    });

    describe("S3 storage", () => {
        const app = express();
        let removeStubS3: SinonStub;

        before(async () => {
            sandbox.stub(config, "storageType").value(StorageType.S3);
            sandbox.stub(config, "s3").value({
                bucketName: "lkod",
                accessKeyId: "testId",
                secretAccessKey: "testSecret",
                endpoint: "http://s3.test.cz",
                s3ForcePathStyle: true,
                signatureVersion: "v4",
            });

            app.use(express.json());
            app.use(express.urlencoded({ extended: true }));

            authRouter = new AuthRouter();
            datasetsRouter = new DatasetsRouter();
            app.use("/auth", authRouter.router);
            app.use("/datasets", datasetsRouter.router);

            app.use((err: any, req: Request, res: Response, next: NextFunction) => {
                const warnCodes = [400, 401, 403, 404, 422];
                const errObject: ICustomErrorObject = HTTPErrorHandler.handle(
                    err,
                    log,
                    warnCodes.includes(err.code) ? "warn" : "error"
                );
                log.silly("Error caught by the router error handler.");
                res.setHeader("Content-Type", "application/json; charset=utf-8");
                res.status(errObject.error_status || 500).send(errObject);
            });
        });

        beforeEach(() => {
            sandbox.stub(S3Service.prototype, "upload").resolves("http://s3.test.cz/lkod/test-org/1234/file01.txt");
            sandbox
                .stub(S3Service.prototype, "list")
                .resolves(["http://s3.test.cz/lkod/test-org/1234/file1.csv", "http://s3.test.cz/lkod/test-org/1234/file2.doc"]);
            removeStubS3 = sandbox.stub(S3Service.prototype, "delete");
        });

        it("should login and create dataset", async () => {
            const loginRes = await supertest(app).post("/auth/login").send({
                email: "test@golemio.cz",
                password: "pass",
            });

            expect(loginRes.status).to.eql(200);
            expect(loginRes.body.accessToken).to.be.a("string");
            accessToken = loginRes.body.accessToken;

            const datasetRes = await supertest(app)
                .post("/datasets")
                .send({
                    organizationId: 1,
                })
                .set("Authorization", `Bearer ${accessToken}`);

            expect(datasetRes.status).to.eql(201);
            expect(datasetRes.body.id).to.be.a("string");
            datasetId = datasetRes.body.id;
        });

        it("should list all dataset files at GET /datasets/:datasetId/files", (done) => {
            supertest(app)
                .get(`/datasets/${datasetId}/files`)
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(200)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    expect(res.body).to.eql({
                        files: [
                            "http://s3.test.cz/lkod/test-org/1234/file1.csv",
                            "http://s3.test.cz/lkod/test-org/1234/file2.doc",
                        ],
                    });
                    done();
                });
        });

        it("should upload file at POST /datasets/:datasetId/files", (done) => {
            supertest(app)
                .post(`/datasets/${datasetId}/files`)
                .set("Authorization", `Bearer ${accessToken}`)
                .attach("datasetFile", path.join(__dirname, "data/file01.txt"))
                .expect(201)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    expect(res.body).to.eql({ file: "http://s3.test.cz/lkod/test-org/1234/file01.txt" });
                    expect(res.headers.location).to.eql("http://s3.test.cz/lkod/test-org/1234/file01.txt");
                    done();
                });
        });

        it("should not allow uploading file at POST /datasets/:datasetId/files", (done) => {
            supertest(app)
                .post(`/datasets/wrongId/files`)
                .set("Authorization", `Bearer ${accessToken}`)
                .attach("datasetFile", path.join(__dirname, "data/file01.txt"))
                .expect(401, done);
        });

        it("should delete file at delete /datasets/:datasetId/files/:filename", (done) => {
            removeStubS3.callsFake(() => null);

            supertest(app)
                .delete(`/datasets/${datasetId}/files/file1.csv`)
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(204, done);
        });

        it("should not allow deleting file at DELETE /datasets/:datasetId/files", (done) => {
            supertest(app)
                .post(`/datasets/wrongId/files`)
                .set("Authorization", `Bearer ${accessToken}`)
                .attach("datasetFile", path.join(__dirname, "data/file01.txt"))
                .expect(401, done);
        });

        it("should return 404 if file does not exist at delete /datasets/:datasetId/files/:filename", (done) => {
            removeStubS3.rejects(new CustomError("File not found", true, "S3Service", 404));

            supertest(app)
                .delete(`/datasets/${datasetId}/files/file1.csv`)
                .set("Authorization", `Bearer ${accessToken}`)
                .expect(404)
                .end((err: Error | CustomError, res: supertest.Response) => {
                    if (err) {
                        done(err);
                    }
                    expect(res.body).to.eql({
                        error_message: "Not Found",
                        error_status: 404,
                    });
                    done();
                });
        });
    });
});
