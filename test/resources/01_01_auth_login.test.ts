import { CustomError, HTTPErrorHandler, ICustomErrorObject } from "@golemio/core/dist/shared/golemio-errors";
import * as chai from "chai";
import * as express from "express";
import { NextFunction, Request, Response } from "express";
import "mocha";
import * as sinon from "sinon";
import * as supertest from "supertest";
import { log } from "../../src/core/helpers";
import { AuthRouter } from "../../src/resources/auth";
import { init } from "./00_init.test";

describe("Login", () => {
    let sandbox: sinon.SinonSandbox;
    // Create clean express instance
    const app = express();
    let authRouter: AuthRouter;
    let accessToken: string;

    before(async () => {
        // mainly database initialization
        await init();

        authRouter = new AuthRouter();

        app.use(express.json());
        app.use(express.urlencoded({ extended: true }));
        // Mount the tested router to the express instance
        app.use("/auth", authRouter.router);
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            // hadle body parser errors
            if (err instanceof SyntaxError) {
                err = new CustomError(err.message, true, "App", (err as any)?.status || 500, err.message);
            }

            const warnCodes = [400, 401, 403, 404, 422];
            const errObject: ICustomErrorObject = HTTPErrorHandler.handle(
                err,
                log,
                warnCodes.includes(err.code) ? "warn" : "error"
            );
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should login user at POST /auth/login", (done) => {
        const url = "/auth/login";
        supertest(app)
            .post(url)
            .send({
                email: "test@golemio.cz",
                password: "pass",
            })
            .expect(200)
            .end((err: Error | CustomError, res: supertest.Response) => {
                if (err) {
                    done(err);
                }
                accessToken = res.body.accessToken;
                chai.expect(accessToken).to.be.a("string");
                chai.expect(res.body.hasDefaultPassword).to.be.eq(false);
                chai.expect(err).to.be.null;
                done();
            });
    });

    it("should throws error if bad data was send at POST /auth/login", (done) => {
        const url = "/auth/login";
        supertest(app)
            .post(url)
            .send({
                email: "test@golemio.cz",
            })
            .expect(400, done);
    });

    it("should throws error if bad data was send at POST /auth/login", (done) => {
        const url = "/auth/login";
        supertest(app)
            .post(url)
            .send({
                password: "pass",
            })
            .expect(400, done);
    });

    it("should throws error if invalid json was send at POST /auth/login", (done) => {
        const url = "/auth/login";
        supertest(app).post(url).send(`{ email: "test@golemio.cz" "password": "pass" }`).type("json").expect(400, done);
    });

    it("should throws error on bad credentials at POST /auth/login", (done) => {
        const url = "/auth/login";
        supertest(app)
            .post(url)
            .send({
                email: "test@golemio.cz",
                password: "badpass",
            })
            .expect(401, done);
    });

    it("should logout user at POST /auth/logout", (done) => {
        const url = "/auth/logout";
        supertest(app).post(url).set("Authorization", `Bearer ${accessToken}`).expect(204, done);
    });

    it("should throws error if token is blacklisted at POST /auth/logout", (done) => {
        const url = "/auth/logout";
        supertest(app).post(url).set("Authorization", `Bearer ${accessToken}`).expect(401, done);
    });

    it("should login new user with default password at POST /auth/login", (done) => {
        const url = "/auth/login";
        supertest(app)
            .post(url)
            .send({
                email: "new@golemio.cz",
                password: "pass",
            })
            .expect(200)
            .end((err: Error | CustomError, res: supertest.Response) => {
                if (err) {
                    done(err);
                }
                accessToken = res.body.accessToken;
                chai.expect(accessToken).to.be.a("string");
                chai.expect(res.body.hasDefaultPassword).to.be.eq(true);
                chai.expect(err).to.be.null;
                done();
            });
    });
});
