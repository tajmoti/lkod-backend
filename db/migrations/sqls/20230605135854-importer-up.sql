alter table "organization" add column "arcGisFeed" text;
comment on column organization."arcGisFeed" is 'URL of ArcGIS feed (DCAT-US 1.1)';

create view "v_organization_with_arcgis_feed" as
select "id", "name", "arcGisFeed"
from "organization"
where "arcGisFeed" is not null;


-- importer user
insert into "user" ("email", "name", "password", "role", "hasDefaultPassword")
values ('importer@lkod', 'LKOD importer', '(none)', 'user', 'false');

create function get_importer_user_id() RETURNS bigint AS $$
    select "id" from "user" where "email" = 'importer@lkod';
$$ LANGUAGE SQL IMMUTABLE;
