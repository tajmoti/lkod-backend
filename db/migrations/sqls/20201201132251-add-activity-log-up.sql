CREATE SEQUENCE activity_log_id_seq
    INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "activity_log"
(
    "id" bigint NOT NULL DEFAULT nextval('activity_log_id_seq'::regclass),
    "scope" character varying(255) COLLATE pg_catalog."default" NOT NULL,
    "action" character varying(255) COLLATE pg_catalog."default" NOT NULL,
    "userId" bigint NOT NULL,
    "didAt" timestamp with time zone NOT NULL DEFAULT now(),
    "meta" json,
    CONSTRAINT activity_log_pkey PRIMARY KEY (id)
);
