CREATE or REPLACE VIEW v_lkod_metadata as 
with 
json_elements as ( 
	select 
		d.id,
		(select
                  jsonb_agg(distinct e ->'formát')::text 
             from jsonb_array_elements("data"->'distribuce') as t(e)) as format,
      	o."name" as organizace,"data"->'klíčové_slovo'#>>'{cs}' as key_words,
      	"data"->'popis'#>>'{cs}'as popis,
      	"data"->'téma'as tema,
      	"data"->'název'#>>'{cs}'as nazev_ds,
      	"data"
     from 
     	dataset d 
     		left join organization o on d."organizationId" = o.id 
     where  status = 'published'),
count_files as (
      select 
      	id, 
      	count (f) as pocet_nahranych_souboru 
      from (
      	select 
      		id,
      		unnest(
      			string_to_array(
      				REGEXP_REPLACE(replace(format, 'http://publications.europa.eu/resource/authority/file-type/', ''),'[\[\]\"]', '','g'),
      			',')) as f 
      	 from   
      		json_elements) as soubory_formaty
      	group by id)
select 
	json_elements.id,
	json_elements.nazev_ds,
	json_elements.popis,
	json_elements.organizace,
	REGEXP_REPLACE (tema::text, '[\[\]\"]', '','g') as tema,
	REGEXP_REPLACE(replace(tema::text, 'http://publications.europa.eu/resource/authority/data-theme/', ''),'[\[\]\"]', '','g')as tema_short,
	REGEXP_REPLACE(replace(format, 'http://publications.europa.eu/resource/authority/file-type/', ''),'[\[\]\"]', '','g')as formaty_dis,
	REGEXP_REPLACE (key_words, '[\[\]\"]', '','g')as klicova_slova, 
	count_files.pocet_nahranych_souboru 
from json_elements
	left join count_files on json_elements.id=count_files.id;
