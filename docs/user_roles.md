# User roles

- admin
- user

Both roles have access to datasets that are only in their organization.

## Admin

User with role `admin` can manage all datasets in its organization.

## User

User with role `user` can manage only datasets which was created by own.
