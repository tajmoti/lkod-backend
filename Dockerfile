ARG BASE_IMAGE=node:18.17.0-alpine

FROM $BASE_IMAGE AS build
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build


FROM $BASE_IMAGE
WORKDIR /app

COPY --from=build /app/node_modules /app/node_modules
COPY --from=build /app/dist /app/dist
COPY bin bin
COPY db db
COPY config config
COPY templates templates
COPY docs/openapi.yaml docs/openapi.yaml
COPY package.json .db-migraterc docker-start.sh ./

# Create a non-root user
RUN addgroup -S nonroot && \
    adduser -S nonroot -G nonroot -h /app -u 1001 -D && \
    chown -R nonroot /app

# Disable persistent history
RUN touch /app/.ash_history && \
    chmod a=-rwx /app/.ash_history && \
    chown root:root /app/.ash_history

USER nonroot

EXPOSE 3000
CMD ["./docker-start.sh"]
