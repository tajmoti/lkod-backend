import { Association, DataTypes, Model, ModelAttributes } from "sequelize";
import { OrganizationModel } from "./OrganizationModel";

export enum UserRole {
    user = "user",
    admin = "admin",
}

export interface IUserAttributes {
    id: number;
    email: string;
    password?: string | null;
    name: string;
    role: UserRole;
    hasDefaultPassword: boolean;
    createdAt: Date;
    updatedAt?: Date | null;

    // associations
    organizations?: OrganizationModel[] | null;
}

export const UserSchema: ModelAttributes = {
    id: {
        autoIncrement: true,
        type: DataTypes.BIGINT,
        primaryKey: true,
        allowNull: false,
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    password: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    role: {
        type: DataTypes.ENUM,
        values: Object.values(UserRole),
        allowNull: false,
        defaultValue: UserRole.user,
    },
    hasDefaultPassword: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true,
    },
};

/**
 * User Model
 * table: user
 */
export class UserModel extends Model<IUserAttributes> implements IUserAttributes {
    public id: number;
    public email: string;
    public password?: string | null;
    public name: string;
    public role: UserRole;
    public hasDefaultPassword: boolean;
    public createdAt: Date;
    public updatedAt?: Date | null;

    public readonly organizations?: OrganizationModel[] | null;

    public static associations: {
        organizations: Association<UserModel, OrganizationModel>;
    };
}
