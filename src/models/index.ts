import { config } from "../core/config";
import { postgresConnector } from "../core/database";
import { ActivityLogModel, ActivityLogSchema } from "./ActivityLogModel";
import { DatasetModel, DatasetSchema } from "./DatasetModel";
import { ForgotPasswordTokenModel, ForgotPasswordTokenSchema } from "./ForgotPasswordTokenModel";
import { OrganizationModel, OrganizationSchema } from "./OrganizationModel";
import { SessionModel, SessionSchema } from "./SessionModel";
import { UserModel, UserSchema } from "./UserModel";
import { UserOrganizationModel, UserOrganizationSchema } from "./UserOrganization";

export const initModels = () => {
    DatasetModel.init(DatasetSchema, {
        schema: config.database.schema,
        sequelize: postgresConnector.getConnection(),
        tableName: "dataset",
    });

    SessionModel.init(SessionSchema, {
        schema: config.database.schema,
        sequelize: postgresConnector.getConnection(),
        tableName: "session",
    });

    OrganizationModel.init(OrganizationSchema, {
        schema: config.database.schema,
        sequelize: postgresConnector.getConnection(),
        tableName: "organization",
    });

    UserModel.init(UserSchema, {
        schema: config.database.schema,
        sequelize: postgresConnector.getConnection(),
        tableName: "user",
    });

    UserOrganizationModel.init(UserOrganizationSchema, {
        schema: config.database.schema,
        sequelize: postgresConnector.getConnection(),
        tableName: "user_organization",
    });

    ActivityLogModel.init(ActivityLogSchema, {
        schema: config.database.schema,
        sequelize: postgresConnector.getConnection(),
        tableName: "activity_log",
    });

    ForgotPasswordTokenModel.init(ForgotPasswordTokenSchema, {
        schema: config.database.schema,
        sequelize: postgresConnector.getConnection(),
        tableName: "forgot_password_token",
    });

    DatasetModel.hasOne(UserModel, {
        as: "user",
        foreignKey: "id",
        sourceKey: "userId",
    });
    DatasetModel.hasOne(OrganizationModel, {
        as: "organization",
        foreignKey: "id",
        sourceKey: "organizationId",
    });

    UserModel.belongsToMany(OrganizationModel, {
        as: "organizations",
        foreignKey: "userId",
        through: "user_organization",
    });
    OrganizationModel.belongsToMany(UserModel, {
        as: "users",
        foreignKey: "organizationId",
        through: "user_organization",
    });

    ForgotPasswordTokenModel.hasOne(UserModel, {
        as: "user",
        foreignKey: "id",
        sourceKey: "userId",
    });
};
