import { Router } from "express";
import { AuthenticationMiddleware } from "../../core/middlewares";
import { BaseRouter } from "../../core/routers/BaseRouter";
import ValidationHelper from "../helpers/ValidationHelper";
import { LookupController } from "./LookupController";

export class LookupRouter extends BaseRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();
    private lookupController: LookupController;

    constructor() {
        super();
        this.lookupController = new LookupController();
        this.initRoutes();
    }

    private initRoutes = (): void => {
        this.router.get(
            "/themes",
            ValidationHelper.getCommonFilters(),
            this.handleValidationError,
            new AuthenticationMiddleware().authenticate,
            this.lookupController.getDatasetThemes
        );
        this.router.get(
            "/publishers",
            ValidationHelper.getCommonFilters(),
            this.handleValidationError,
            new AuthenticationMiddleware().authenticate,
            this.lookupController.getPublishers
        );
        this.router.get(
            "/formats",
            ValidationHelper.getCommonFilters(),
            this.handleValidationError,
            new AuthenticationMiddleware().authenticate,
            this.lookupController.getFormats
        );
        this.router.get(
            "/keywords",
            ValidationHelper.getCommonFilters(),
            this.handleValidationError,
            new AuthenticationMiddleware().authenticate,
            this.lookupController.getKeywords
        );
        this.router.get(
            "/statuses",
            ValidationHelper.getCommonFilters(),
            this.handleValidationError,
            new AuthenticationMiddleware().authenticate,
            this.lookupController.getStatuses
        );
    };
}
