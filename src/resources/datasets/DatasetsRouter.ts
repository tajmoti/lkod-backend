import { NextFunction, RequestHandler, Response, Router } from "express";
import * as fileUpload from "express-fileupload";
import { body, param } from "express-validator";
import slug from "slugify";
import { config } from "../../core/config";
import { AuthenticationMiddleware, IExtendedRequest } from "../../core/middlewares";
import { fileAccessMiddleware } from "../../core/middlewares/FilePermissionsMiddleware";
import { BaseRouter } from "../../core/routers/BaseRouter";
import { IFile } from "../../core/storage/Storage";
import { DatasetStatusToSet } from "../../models/DatasetModel";
import QueryHelper from "../helpers/QueryHelper";
import ValidationHelper from "../helpers/ValidationHelper";
import { DatasetsController } from "./";
import { DatasetFilesController } from "./DatasetFilesController";
import { IDatasetFilter, IDatasetQueryParams } from "./interfaces/IDatasetQueryParams";

/**
 * Dataset Router class
 */
export class DatasetsRouter extends BaseRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();
    public controller: DatasetsController;
    private filesController: DatasetFilesController;

    constructor() {
        super();
        this.controller = new DatasetsController();
        this.filesController = new DatasetFilesController();

        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.get(
            "/",
            ValidationHelper.getCommonFilters(),
            this.handleValidationError,
            new AuthenticationMiddleware().authenticate,
            this.getAllDatasets
        );
        this.router.post(
            "/",
            [body("organizationId").notEmpty().isNumeric()],
            this.handleValidationError,
            new AuthenticationMiddleware().authenticate,
            this.createNewDataset
        );
        this.router.get(
            "/:datasetId",
            [param("datasetId").notEmpty().isString()],
            this.handleValidationError,
            new AuthenticationMiddleware().authenticate,
            this.getDatasetById
        );
        this.router.patch(
            "/:datasetId",
            [
                param("datasetId").notEmpty().isString(),
                body("status")
                    .notEmpty()
                    .custom((value, { req }) => Object.values(DatasetStatusToSet).includes(value)),
            ],
            this.handleValidationError,
            new AuthenticationMiddleware().authenticate,
            this.updateDataset
        );
        this.router.delete(
            "/:datasetId",
            [param("datasetId").notEmpty().isString()],
            this.handleValidationError,
            new AuthenticationMiddleware().authenticate,
            this.deleteDataset
        );
        this.router.get("/:datasetId/files", new AuthenticationMiddleware().authenticate, this.listFiles);
        this.router.post(
            "/:datasetId/files",
            new AuthenticationMiddleware().authenticate,
            fileAccessMiddleware,
            fileUpload({
                preserveExtension: true,
                limits: {
                    fileSize: config.storageFileMaxSizeInKB * 1024,
                },
                abortOnLimit: true,
            }),
            this.uploadFile
        );
        this.router.delete(
            "/:datasetId/files/:filename",
            new AuthenticationMiddleware().authenticate,
            fileAccessMiddleware,
            this.deleteFile
        );
    };

    /**
     * Getting all datasets
     *
     * @param {IExtendedRequest} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private getAllDatasets: RequestHandler = async (
        req: IExtendedRequest<unknown, unknown, unknown, IDatasetQueryParams>,
        res: Response,
        next: NextFunction
    ): Promise<void> => {
        try {
            const filter: IDatasetFilter = {
                filter: {
                    ...(req.query.organizationId && { organizationId: +req.query.organizationId }),
                    status: QueryHelper.parseParam(req.query.status),
                    searchString: req.query.searchString,
                    publisherIri: req.query.publisherIri,
                    themeIris: QueryHelper.parseParam(req.query.themeIris),
                    keywords: QueryHelper.parseParam(req.query.keywords),
                    formatIris: QueryHelper.parseParam(req.query.formatIris),
                },
                ...(req.query.limit && { limit: parseInt(req.query.limit, 10) }),
                ...(req.query.offset && { offset: parseInt(req.query.offset, 10) }),
            };

            const { datasets, totalCount } = await this.controller.getAllDatasets(req.decoded?.id || 0, filter);
            res.setHeader("X-Total-Count", totalCount);
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(200).json(datasets);
        } catch (err) {
            next(err);
        }
    };

    /**
     * Creating new dataset
     *
     * @param {IExtendedRequest} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private createNewDataset = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const dataset = await this.controller.createNewDataset(req.decoded?.id || 0, req.body.organizationId);
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(201).json(dataset);
        } catch (err) {
            next(err);
        }
    };

    /**
     * Getting one dataset
     *
     * @param {IExtendedRequest} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private getDatasetById = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const dataset = await this.controller.getDatasetById(req.params.datasetId, req.decoded?.id || 0);
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(200).json(dataset);
        } catch (err) {
            next(err);
        }
    };

    /**
     * Updating dataset
     *
     * @param {IExtendedRequest} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private updateDataset = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const dataset = await this.controller.updateDataset(req.params.datasetId, req.body.status, req.decoded?.id || 0);
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(200).json(dataset);
        } catch (err) {
            next(err);
        }
    };

    /**
     * Deleting dataset
     *
     * @param {IExtendedRequest} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private deleteDataset = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.controller.deleteDataset(req.params.datasetId, req.decoded?.id || 0);
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    /**
     * List all dataset files
     *
     * @param {IExtendedRequest} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private listFiles = async (req: IExtendedRequest, res: Response, next: NextFunction) => {
        try {
            const files = await this.filesController.listFiles(req.params.datasetId);
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(200).json({ files });
        } catch (err) {
            next(err);
        }
    };

    /**
     * Upload file for dataset
     *
     * @param {IExtendedRequest} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private uploadFile = async (req: IExtendedRequest, res: Response, next: NextFunction) => {
        try {
            if (!req.files?.datasetFile) {
                return res.status(400).json({ error: "File not attached." });
            }

            if (Array.isArray(req.files.datasetFile)) {
                return res.status(400).json({ error: "Multipart not allowed" });
            }

            const file: IFile = {
                name: slug(req.files.datasetFile.name, { lower: true, remove: /[*+~()'"!:@]/g }),
                data: req.files.datasetFile.data,
            };

            const filepath = await this.filesController.uploadFile(req.params.datasetId, file, req.decoded?.id || 0);
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.setHeader("Location", filepath);
            res.status(201).json({ file: filepath });
        } catch (err) {
            next(err);
        }
    };

    /**
     * Delete dataset file
     *
     * @param {IExtendedRequest} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private deleteFile = async (req: IExtendedRequest, res: Response, next: NextFunction) => {
        try {
            await this.filesController.deleteFile(req.params.datasetId, req.params.filename, req.decoded?.id || 0);
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
}
