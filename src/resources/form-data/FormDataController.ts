import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { literal, Op } from "sequelize";
import slug from "slugify";
import { config } from "../../core/config";
import { dataValidator, logActivity, tokenManager } from "../../core/helpers";
import { IDecodedToken } from "../../core/middlewares";
import { SparqlConnectionStrategy } from "../../core/strategies";
import { DatasetModel, DatasetStatus, IDatasetData } from "../../models/DatasetModel";
import { SessionModel } from "../../models/SessionModel";

/**
 * Form Data Controller class to handle input data from external form
 */
export class FormDataController {
    /**
     * Receives form data
     *
     * @param {{ userData: string, formData: string }} data
     */
    public async receiveFormData(data: { userData: string; formData: string }): Promise<{ redirectRelativeUrl: string }> {
        try {
            const { accessToken, sessionId, datasetId, backlinkUrlPath } = JSON.parse(data.userData);

            // accessToken validation
            const decoded = (await tokenManager.verifyAndDecodeToken(accessToken)) as IDecodedToken;

            // userData validation
            await this.validateUserData(sessionId, datasetId, decoded.id);

            // formData save
            await this.saveFormData(datasetId, data.formData);

            // session delete
            await SessionModel.destroy({
                where: {
                    id: sessionId,
                },
            });

            await logActivity({
                scope: "FormData",
                action: "receive",
                userId: decoded.id,
                meta: {
                    datasetId,
                    sessionId,
                },
            });

            return {
                redirectRelativeUrl: backlinkUrlPath,
            };
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError("Error while receiving form data", true, this.constructor.name, 500, err);
            }
        }
    }

    /**
     * Validates user data (authorization)
     *
     * @param {string} sessionId UUID
     * @param {string} datasetId UUID
     * @param {number} userId
     */
    private async validateUserData(sessionId: string, datasetId: string, userId: number): Promise<boolean> {
        try {
            const session = await SessionModel.findOne({
                where: {
                    [Op.and]: [
                        {
                            datasetId,
                            id: sessionId,
                            userId,
                        },
                        literal(`"createdAt" + ("expiresIn" * INTERVAL '1 second') > now()`),
                    ],
                },
            });

            if (!session) {
                throw new Error("Session not exists or is expired");
            }

            return true;
        } catch (err) {
            throw new CustomError("Error while userData validation", true, this.constructor.name, 400, err);
        }
    }

    /**
     * Saves form data into dataset
     *
     * @param {string} datasetId UUID
     * @param {string} formData Stringified JSON
     * @returns {boolean}
     */
    private async saveFormData(datasetId: string, formData: string): Promise<boolean> {
        try {
            const dataset = await DatasetModel.findOne({
                include: [DatasetModel.associations.organization],
                where: {
                    id: datasetId,
                    status: {
                        [Op.ne]: DatasetStatus.deleted,
                    },
                },
            });

            if (!dataset) {
                throw new Error("Dataset not exists");
            }

            dataset.data = JSON.parse(formData) as IDatasetData;

            // enriching data by custom information
            dataset.data.iri = `${config.backend_url}${config.pathPrefix}/lod/catalog/${dataset.id}`;
            dataset.data.poskytovatel = `${config.linked_data.poskytovatel_url_prefix}${dataset.organization.slug}`;

            const distribIriDuplicityCheck = new Map();
            if (dataset.data?.distribuce) {
                dataset.data.distribuce = dataset.data.distribuce.map((distrib: any, index: number) => {
                    // empty distribution (work in progress)
                    if (["", "./"].includes(distrib.přístupové_url)) {
                        return distrib;
                    }

                    // use existing iri (update)
                    let iri = distrib.iri
                        ? distrib.iri
                        : `${dataset.data?.iri}/distributions/${this.resolveDistributionIriSufix(distrib)}`;

                    if (distribIriDuplicityCheck.has(iri)) {
                        let i = 1;
                        while (true) {
                            if (distribIriDuplicityCheck.has(`${iri}_${i}`)) {
                                i++;
                            } else {
                                iri = `${iri}_${i}`;
                                break;
                            }
                        }
                    }
                    distribIriDuplicityCheck.set(iri, 1);

                    if (distrib.přístupová_služba) {
                        distrib.přístupová_služba = {
                            ...distrib.přístupová_služba,
                            iri: `${iri}/DataService`,
                        };
                    }

                    return {
                        ...distrib,
                        iri,
                    };
                });
            }

            dataset.changed("data", true); // force update column data
            // /enriching data by custom information

            // data validation
            dataset.validationResult = dataValidator.validate(dataset.data);
            dataset.changed("validationResult", true); // force update column validationResult
            // /data validation

            if (dataset.status === DatasetStatus.created) {
                dataset.status = DatasetStatus.saved;
            }
            dataset.updatedAt = new Date();
            await dataset.save();

            // upload dataset data into sparql
            if (dataset.status === DatasetStatus.published) {
                await SparqlConnectionStrategy.removeDatasetCascade(dataset?.data?.iri || "");
                await SparqlConnectionStrategy.uploadDataset(dataset.data);
            }

            return true;
        } catch (err) {
            throw new CustomError("Error while saving formData", true, this.constructor.name, 400, err);
        }
    }

    private resolveDistributionIriSufix(distribution: any): string {
        let type = "none";
        let urlSlug = "none";

        if (distribution.formát) {
            type = distribution.formát.substring(distribution.formát.lastIndexOf("/") + 1);
            type = type.toLowerCase();
        } else {
            type = "url";
        }

        if (distribution.přístupové_url) {
            urlSlug = slug(distribution.přístupové_url, { lower: true, remove: /[*+~()'"!:@]/g }).replace(/\//g, "_");
            urlSlug = urlSlug.slice(-20);
        }

        return `${type}-${urlSlug}`;
    }
}
