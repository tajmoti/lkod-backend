import { NextFunction, Request, Response, Router } from "express";
import { body } from "express-validator";
import { config } from "../../core/config";
import { BaseRouter } from "../../core/routers/BaseRouter";
import { FormDataController } from "./";

/**
 * Form Data Router class
 */
export class FormDataRouter extends BaseRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();
    public controller: FormDataController;

    constructor() {
        super();
        this.controller = new FormDataController();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.post(
            "/",
            [body("userData").notEmpty().isString(), body("formData").notEmpty().isString()],
            this.handleValidationError,
            this.receiveFormData
        );
    };

    /**
     * Receiving form data
     *
     * @param {Request} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private receiveFormData = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const result = await this.controller.receiveFormData(req.body);
            const redirectPath = new URL(result.redirectRelativeUrl ?? "", config.frontend_url).href;
            res.redirect(redirectPath);
        } catch (err) {
            next(err);
        }
    };
}
