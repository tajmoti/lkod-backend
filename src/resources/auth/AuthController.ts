import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { literal, Op } from "sequelize";
import { v4, validate as uuidValidate, version as uuidVersion } from "uuid";
import { config } from "../../core/config";
import { logActivity, secretManager, tokenManager } from "../../core/helpers";
import { Mailer } from "../../core/mailer/Mailer";
import { ForgotPasswordTokenModel, IForgotPasswordTokenAttributes } from "../../models/ForgotPasswordTokenModel";
import { IUserAttributes, UserModel } from "../../models/UserModel";

/**
 * Auth Controller class
 */
export class AuthController {
    private mailer: Mailer;

    constructor() {
        this.mailer = new Mailer();
    }

    /**
     * Logs in the user
     *
     * @param {string} email
     * @param {string} password
     * @returns {string} Access token
     */
    public async login(email: string, password: string): Promise<{ accessToken: string; hasDefaultPassword: boolean }> {
        try {
            const user = await UserModel.findOne({
                where: {
                    email,
                },
            });

            const passMatched = await secretManager.checkSecret(password, user?.password || "");
            if (!user || !passMatched) {
                throw new CustomError(`User was not found or email and password did not match`, true, this.constructor.name, 401);
            }

            const { password: pass, ...userToToken } = user.toJSON() as IUserAttributes;
            const token = await tokenManager.generateToken(userToToken, config.token_expires_in);

            return { accessToken: token, hasDefaultPassword: user.hasDefaultPassword };
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError("Error while user login", true, this.constructor.name, 500, err);
            }
        }
    }

    /**
     * Log out the user. Access token is blacklisted
     *
     * @param {string} jti Access token internal id
     * @param {number} exp Access token expiration
     */
    public async logout(jti: string, exp?: number): Promise<void> {
        try {
            await tokenManager.setTokenToBlacklist(jti, exp);
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError("Error while user logout", true, this.constructor.name, 500, err);
            }
        }
    }

    /**
     * Change user's password
     */
    public async changePassword(userId: number, currentPassword: string, newPassword: string): Promise<void> {
        try {
            const user = await UserModel.findByPk(userId);
            const passMatched = await secretManager.checkSecret(currentPassword, user?.password || "");
            if (!user || !passMatched) {
                throw new CustomError(`User was not found or current password did not match`, true, this.constructor.name, 401);
            }

            user.password = await secretManager.hashSecret(newPassword);
            user.hasDefaultPassword = false;
            user.updatedAt = new Date();
            await user.save();

            await logActivity({
                scope: "User",
                action: "change-password",
                userId,
            });
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError("Error while user change password", true, this.constructor.name, 500, err);
            }
        }
    }

    /**
     * Forgot password
     * generate token and send email with reset link
     */
    public async forgotPassword(email: string): Promise<void> {
        try {
            const user = await UserModel.findOne({
                where: {
                    email,
                },
            });
            if (!user) {
                throw new CustomError(`User was not found`, true, this.constructor.name, 401);
            }

            const validTo = new Date();
            validTo.setDate(validTo.getDate() + 1); // now + 1 day

            const newToken = await ForgotPasswordTokenModel.create({
                id: v4(),
                userId: user.id,
                validTo,
            } as IForgotPasswordTokenAttributes);

            const url = `${config.frontend_url}/forgot-password/${newToken.id}`;
            await this.mailer.sendPasswordReset(user.email, url);

            await logActivity({
                scope: "User",
                action: "forgot-password",
                userId: user.id,
                meta: {
                    tokenId: newToken.id,
                },
            });
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError("Error while requesting forgot password", true, this.constructor.name, 500, err);
            }
        }
    }

    /**
     * Reset password
     * using forgot password token
     */
    public async resetPassword(tokenId: string, newPassword: string): Promise<void> {
        try {
            if (!uuidValidate(tokenId) || uuidVersion(tokenId) !== 4) {
                throw new CustomError(`Invalid forgot password token`, true, this.constructor.name, 400);
            }

            const forgotPasswordToken = await ForgotPasswordTokenModel.findOne({
                where: {
                    [Op.and]: [
                        {
                            id: tokenId,
                        },
                        literal(`"validTo" > now()`),
                    ],
                },
                include: [ForgotPasswordTokenModel.associations.user],
            });
            if (!forgotPasswordToken) {
                throw new CustomError(`Forgot password token was not found or expired`, true, this.constructor.name, 401);
            }

            forgotPasswordToken.user.password = await secretManager.hashSecret(newPassword);
            forgotPasswordToken.user.hasDefaultPassword = false;
            forgotPasswordToken.user.updatedAt = new Date();
            await forgotPasswordToken.user.save();

            await ForgotPasswordTokenModel.destroy({
                where: {
                    userId: forgotPasswordToken.userId,
                },
            });

            await logActivity({
                scope: "User",
                action: "reset-password",
                userId: forgotPasswordToken.userId,
                meta: {
                    tokenId: forgotPasswordToken.id,
                },
            });
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError("Error while reset password", true, this.constructor.name, 500, err);
            }
        }
    }
}
