import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { literal, Op } from "sequelize";
import { v4 } from "uuid";
import { config } from "../../core/config";
import { logActivity } from "../../core/helpers";
import { DatasetModel, DatasetStatus } from "../../models/DatasetModel";
import { ISessionAttributes, SessionModel } from "../../models/SessionModel";
import { UserModel, UserRole } from "../../models/UserModel";

/**
 * Sessions Controller class
 * Used to handle communication with external form
 */
export class SessionsController {
    /**
     * Deletes old sessions
     */
    public static async deleteOldSessions(): Promise<void> {
        await SessionModel.destroy({
            where: literal(`"createdAt" + ("expiresIn" * INTERVAL '1 second') <= now()`),
        });
    }

    /**
     * Creates new session. User id is used for authorization
     *
     * @param {string} datasetId UUID
     * @param {number} userId
     */
    public async createNewSession(datasetId: string, userId: number): Promise<ISessionAttributes> {
        try {
            const user = await UserModel.findByPk(userId, {
                include: [UserModel.associations.organizations],
            });

            let dataset: DatasetModel | null = null;

            switch (user?.role) {
                case UserRole.user:
                    dataset = await DatasetModel.findOne({
                        where: {
                            id: datasetId,
                            organizationId: {
                                [Op.in]: user?.organizations?.map((o) => o.id),
                            },
                            status: {
                                [Op.ne]: DatasetStatus.deleted,
                            },
                        },
                    });
                    break;
                case UserRole.admin:
                    dataset = await DatasetModel.findOne({
                        where: {
                            id: datasetId,
                            status: {
                                [Op.ne]: DatasetStatus.deleted,
                            },
                        },
                    });
                    break;
                default:
                    break;
            }

            if (!dataset) {
                throw new CustomError(`Dataset with id ${datasetId} was not found`, true, this.constructor.name, 404);
            }

            const session = await SessionModel.create({
                datasetId,
                expiresIn: config.session_expires_in,
                id: v4(),
                userId,
            } as ISessionAttributes);

            await logActivity({
                scope: "Session",
                action: "create",
                userId,
                meta: {
                    datasetId,
                    sessionId: session.id,
                },
            });

            return (session.toJSON ? session.toJSON() : session) as ISessionAttributes;
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError("Error while creating new Session", true, this.constructor.name, 500, err);
            }
        }
    }
}
