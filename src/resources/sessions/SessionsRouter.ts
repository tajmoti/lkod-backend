import { NextFunction, Response, Router } from "express";
import { body } from "express-validator";
import { AuthenticationMiddleware, IExtendedRequest } from "../../core/middlewares";
import { BaseRouter } from "../../core/routers/BaseRouter";
import { SessionsController } from "./";

/**
 * Sessions Router class
 */
export class SessionsRouter extends BaseRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();
    public controller: SessionsController;

    constructor() {
        super();
        this.controller = new SessionsController();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.post(
            "/",
            [
                body("datasetId")
                    .notEmpty()
                    .matches(/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i),
            ],
            this.handleValidationError,
            new AuthenticationMiddleware().authenticate,
            this.createNewSession
        );
    };

    /**
     * Creating new session
     *
     * @param {IExtendedRequest} req
     * @param {Response} res
     * @param {NextFunction} next
     * @returns {Promise<void>}
     */
    private createNewSession = async (req: IExtendedRequest, res: Response, next: NextFunction): Promise<void> => {
        try {
            const session = await this.controller.createNewSession(req.body.datasetId, req.decoded?.id || 0);
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(201).json(session);
        } catch (err) {
            next(err);
        }
    };
}
