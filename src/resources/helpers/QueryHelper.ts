import * as sequelize from "sequelize";
import { DatasetStatus, IDatasetAttributes } from "../../models/DatasetModel";
import { UserModel, UserRole } from "../../models/UserModel";
import { IDatasetFilter } from "../datasets/interfaces/IDatasetQueryParams";
import SearchHelper from "./SearchHelper";

export default class QueryHelper {
    public static parseParam(param: string | string[] | undefined): undefined | string[] {
        return param ? (Array.isArray(param) ? param : [param]) : undefined;
    }

    public static buildWherePart(params: IDatasetFilter | undefined, user: UserModel | null) {
        let where: sequelize.FindOptions<IDatasetAttributes>["where"] = {
            ...(params?.filter?.organizationId && { organizationId: params?.filter?.organizationId }),
            ...(params?.filter?.status
                ? { status: { [sequelize.Op.in]: params?.filter?.status } }
                : { status: { [sequelize.Op.ne]: DatasetStatus.deleted } }),
        };

        const whereParts = [];

        if (params?.filter?.searchString) {
            const searchTokens = SearchHelper.tokenizeSearchString(params?.filter?.searchString);
            whereParts.push(
                sequelize.literal(
                    "(" +
                        searchTokens
                            .map((token) => {
                                return (
                                    `unaccent('unaccent', data -> 'název' ->> 'cs') ILIKE unaccent('unaccent', '%${token}%')` +
                                    ` OR ` +
                                    `unaccent('unaccent', data -> 'popis' ->> 'cs') ILIKE unaccent('unaccent', '%${token}%')` +
                                    ` OR ` +
                                    `"data"->'klíčové_slovo' @> '{"cs": ["${token}"]}'`
                                );
                            })
                            .join(" OR ") +
                        ")"
                )
            );
        }

        if (params?.filter?.keywords) {
            whereParts.push(
                sequelize.literal(
                    "(" +
                        params!.filter.keywords
                            .map((keyword) => {
                                return `"data"->'klíčové_slovo' @> '{"cs": ["${keyword}"]}'`;
                            })
                            .join("OR") +
                        ")"
                )
            );
        }

        if (params?.filter?.publisherIri) {
            whereParts.push({ "data.poskytovatel": params.filter.publisherIri });
        }

        if (params?.filter?.themeIris) {
            whereParts.push(
                sequelize.literal(
                    "(" +
                        params!
                            .filter!.themeIris!.map((el) => {
                                return `"data"->'téma' @> '"${el}"'`;
                            })
                            .join("OR") +
                        ")"
                )
            );
        }

        if (params?.filter?.formatIris) {
            whereParts.push(
                sequelize.literal(
                    "(" +
                        params!
                            .filter!.formatIris!.map((format) => {
                                return `"data"->'distribuce' @> '[{"formát": "${format}"}]'`;
                            })
                            .join("OR") +
                        ")"
                )
            );
        }

        if (user?.role === UserRole.user) {
            where = {
                ...where,
                organizationId: params?.filter?.organizationId
                    ? // filter is not empty
                      user?.organizations?.map((o) => o.id).includes(params?.filter?.organizationId)
                        ? // filter is in user's organizations
                          params?.filter?.organizationId
                        : // filter not match user's organizations
                          { [sequelize.Op.in]: [] }
                    : // empty filter
                      { [sequelize.Op.in]: user?.organizations?.map((o) => o.id) },
            };
        }

        const concatedWhere = {
            ...where,
            [sequelize.Op.and]: whereParts,
        };

        return concatedWhere;
    }
}
