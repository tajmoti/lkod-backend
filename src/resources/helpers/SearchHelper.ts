export default class SearchHelper {
    public static tokenizeSearchString = (searchString: string) => {
        return searchString
            .trim()
            .replace(/[-_&\/\\^$#'".,~%:*+?!|()[\]<>{}]/g, "")
            .split(/\s+/)
            .map((word) => word.normalize("NFD"));
    };
}
