import { NextFunction, Request, Response, Router } from "express";
import { config } from "../../core/config";
import { log } from "../../core/helpers";
import { BaseRouter } from "../../core/routers/BaseRouter";
import { StorageType } from "../../core/storage/Storage";

export class DefaultRouter extends BaseRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();

    constructor(private commitSHA: string) {
        super();
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.get(["/", "/health", "/status"], (req: Request, res: Response, _: NextFunction) => {
            log.silly("Health check/status called.");
            res.json({
                app_name: "Golemio LKOD Backend API",
                commit_sha: this.commitSHA,
                status: "Up",
                // Current app version (from environment variable) according to package.json version
                version: config.app_version,
            });
        });

        this.router.get("/status/files", (req: Request, res: Response, _: NextFunction) => {
            if (config.storageType !== StorageType.NONE) {
                res.sendStatus(200);
            } else {
                res.status(503).json({ error_message: "Storage unavailable" });
            }
        });
    };
}
