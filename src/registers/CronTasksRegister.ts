import { ErrorHandler } from "@golemio/core/dist/shared/golemio-errors";
import { config } from "../core/config";
import { cronTasksManger, log } from "../core/helpers";
import { SparqlConnectionStrategy } from "../core/strategies";
import { OrganizationRepository } from "../models/OrganizationRepository";
import { DatasetsController } from "../resources/datasets";
import { SessionsController } from "../resources/sessions";
import RefreshArcgisDatasetsTask from "./tasks/RefreshArcgisDatasetsTask";

class CronTasksRegister {
    private organizationRepository: OrganizationRepository;

    constructor() {
        this.organizationRepository = new OrganizationRepository();
    }

    public registerCronTasks = (): void => {
        try {
            /// Periodical old session deleting
            cronTasksManger.registerTask({
                cronTime: "0 * * * * *", // at every minute
                name: `delete_old_sessions`,
                process: async () => {
                    await SessionsController.deleteOldSessions();
                    log.verbose(`Old sessions deleted.`);
                },
            });
        } catch (err: any) {
            ErrorHandler.handle(err, log, "error");
        }

        try {
            /// Periodical upload sparql vocabularies
            cronTasksManger.registerTask({
                cronTime: config.sparql.vocabulary.crontime,
                name: `sparql_upload_rdf_static_data`,
                process: async () => {
                    await SparqlConnectionStrategy.uploadVocabularies(config.sparql.vocabulary.static_data);
                },
            });
        } catch (err: any) {
            ErrorHandler.handle(err, log, "error");
        }

        try {
            /// Periodical upload Organizations data to sparql
            cronTasksManger.registerTask({
                cronTime: config.sparql.vocabulary.organizations_crontime,
                name: `sparql_upload_organizations`,
                process: async () => {
                    const jsonLd = await this.organizationRepository.getOrganizationsAsJsonLD();
                    await SparqlConnectionStrategy.removeDatasetCascade(config.linked_data.poskytovatel_url_prefix);
                    await SparqlConnectionStrategy.uploadDataset(jsonLd); //
                },
            });
        } catch (err: any) {
            ErrorHandler.handle(err, log, "error");
        }

        try {
            /// Periodical delete old datasets in "created" state
            cronTasksManger.registerTask({
                cronTime: "0 0 * * * *", // every hour
                name: `delete_old_created_datasets`,
                process: async () => {
                    await DatasetsController.deleteOldCreatedDatasets();
                    log.verbose(`Old created datasets deleted.`);
                },
            });
        } catch (err: any) {
            ErrorHandler.handle(err, log, "error");
        }

        try {
            cronTasksManger.registerTask(new RefreshArcgisDatasetsTask(this.organizationRepository, log));
        } catch (err: any) {
            ErrorHandler.handle(err, log, "error");
        }
    };
}

const cronTasksRegister = new CronTasksRegister();

export { cronTasksRegister };
