import { config } from "../../config";
import SparqlCatalogRepository from "../../database/SparqlCatalogRepository";
import { CatalogNamespace } from "./CatalogNamespace";
import { IListFormat } from "./interfaces/IListFormat";
import { IListKeyword } from "./interfaces/IListKeyword";
import { IListPublisher } from "./interfaces/IListPublisher";
import { IListTheme } from "./interfaces/IListTheme";

export class SparqlLookupHelper {
    private lang: string = "cs";
    private repository: SparqlCatalogRepository;

    constructor() {
        this.repository = new SparqlCatalogRepository(config.sparql);
    }

    public getPublishers = (): Promise<IListPublisher[]> => {
        const query = `${this.createPrefixes(["foaf"])}
            SELECT ?iri (?labels AS ?label)
            WHERE {
                ?iri foaf:name ?labels .
                FILTER ( strstarts(str(?iri), '${config.linked_data.poskytovatel_url_prefix}') )
                FILTER(LANG(?labels) = '${this.lang}').
            }`;

        return this.repository.query<IListPublisher>(query);
    };

    public getThemes = async (): Promise<IListTheme[]> => {
        const query = `${this.createPrefixes(["skos"])}
            SELECT ?iri (?prefLabel AS ?label)
            WHERE {
                ?iri skos:prefLabel ?prefLabel .
                FILTER ( strstarts(str(?iri), 'http://publications.europa.eu/resource/authority/data-theme/') )
                FILTER(LANG(?prefLabel) = '${this.lang}').
            }`;

        return this.repository.query(query);
    };

    public getKeywords = async (): Promise<IListKeyword[]> => {
        const query = `${this.createPrefixes()}
            SELECT ?label
            WHERE {
                ?iri dcat:keyword ?label.
                FILTER(LANG(?label) = '${this.lang}').
            } GROUP BY ?label`;
        return this.repository.query(query);
    };

    public getFormats = async (): Promise<IListFormat[]> => {
        const query = `${this.createPrefixes(["skos"])}
            SELECT ?iri (?prefLabel AS ?label)
            WHERE {
                ?iri skos:prefLabel ?prefLabel .
                FILTER ( strstarts(str(?iri), 'http://publications.europa.eu/resource/authority/file-type/') )
                FILTER(LANG(?prefLabel) = 'en') .
            }`;
        return this.repository.query(query);
    };

    private createPrefixes = (prefixes?: Array<keyof typeof CatalogNamespace>): string => {
        if (!prefixes) prefixes = <Array<keyof typeof CatalogNamespace>>Object.keys(CatalogNamespace);

        return prefixes.map((name) => `PREFIX ${name}: <${CatalogNamespace[name]}>`).join("\n");
    };
}
