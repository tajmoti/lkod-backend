export enum CatalogNamespace {
    dct = "http://purl.org/dc/terms/",
    dcat = "http://www.w3.org/ns/dcat#",
    skos = "http://www.w3.org/2004/02/skos/core#",
    foaf = "http://xmlns.com/foaf/0.1/",
    iana = "http://www.iana.org/assignments/media-types/",
    pu = "https://data.gov.cz/slovník/podmínky-užití/",
    vcard = "http://www.w3.org/2006/vcard/ns#",
    gr = "http://purl.org/goodrelations/v1#",
    owl = "http://www.w3.org/2002/07/owl#",
    schema = "http://schema.org/",
}
