import CachedLookups, { LookupFilter } from "./CachedLookups";
import { IDatasetQueryOptions } from "./interfaces/IDatasetQueryOptions";
import { IListFormat } from "./interfaces/IListFormat";
import { IListKeyword } from "./interfaces/IListKeyword";
import { IListPublisher } from "./interfaces/IListPublisher";
import { IListTheme } from "./interfaces/IListTheme";
import { PostgresLookupHelper } from "./PostgresLookupHelper";

export default class LookupFacade {
    private lookupHelper: CachedLookups;
    private postgresLookupHelper: PostgresLookupHelper;

    constructor() {
        this.lookupHelper = CachedLookups.getInstance();
        this.postgresLookupHelper = PostgresLookupHelper.getInstance();
    }

    public getPublishers = async (userId: number, options: IDatasetQueryOptions): Promise<IListPublisher[]> => {
        const result = await this.postgresLookupHelper.getPublishers(userId, options);
        const lookup = await this.lookupHelper.getLookup(LookupFilter.publishers);

        return result
            .map((el) => {
                return {
                    iri: el.iri,
                    count: el.count,
                    label: lookup.find((publisher) => publisher.iri === el.iri)?.label,
                } as IListPublisher;
            })
            .filter((el) => el.label);
    };

    public getThemes = async (userId: number, options: IDatasetQueryOptions): Promise<IListTheme[]> => {
        const result = await this.postgresLookupHelper.getThemes(userId, options);
        const lookup = await this.lookupHelper.getLookup(LookupFilter.themes);

        return result
            .map<IListTheme>((el) => {
                return {
                    iri: el.iri,
                    count: el.count,
                    label: lookup.find((publisher) => publisher.iri === el.iri)?.label,
                };
            })
            .filter((el) => el.label);
    };

    public getFormats = async (userId: number, options: IDatasetQueryOptions): Promise<IListFormat[]> => {
        const result = await this.postgresLookupHelper.getFormats(userId, options);
        const lookup = await this.lookupHelper.getLookup(LookupFilter.formats);

        return result
            .map<IListFormat>((el) => {
                return {
                    iri: el.iri,
                    count: el.count,
                    label: lookup.find((publisher) => publisher.iri === el.iri)?.label,
                };
            })
            .filter((el) => el.label);
    };

    public getKeywords = async (userId: number, options: IDatasetQueryOptions): Promise<IListKeyword[]> => {
        const result = await this.postgresLookupHelper.getKeywords(userId, options);

        return result.filter((el) => el.label) as IListKeyword[];
    };

    public getStatuses = async (userId: number, options: IDatasetQueryOptions): Promise<IListKeyword[]> => {
        return await this.postgresLookupHelper.getStatuses(userId, options);
    };
}
